//: Playground - noun: a place where people can play

import Foundation

typealias Vertex = Int
typealias Connection = (a: Vertex, b: Vertex)
typealias ModifiedGraph = (result: Graph, neededChange: Bool)
typealias VertexBunch = Set<Vertex>
typealias TraversalResult = (success: Bool, reached: VertexBunch) //Maybe this should be a set, as there are no guarantees about consistent ordering process

func ==(lhs: Graph, rhs: Graph) -> Bool {
    return lhs.relations == rhs.relations
}

extension Array {
    func arrayWithValueAtIndex(index: Int, replacedWith: T) -> [T]{
        let left = self.upToIndex(index)
        let right = self.fromIndex(index + 1)
        
        return left + [replacedWith] + right
    }
    
    func upToIndex(index: Int) -> [T] {
        return Array(self[0 ..< min(index, self.count)])
    }
    
    func fromIndex(index: Int) -> [T] {
        return Array(self[min(self.count, index) ..< self.count])
    }
    
}

extension Set {
    init(_ singleton: T){
        self.init([singleton])
    }
}

struct Graph {
    
    let relations: [[Bool]]
    
    var vertices: VertexBunch {
        return VertexBunch(Array(1..<relations.count))
    }
    
    init(size: Int) {
        relations = [[Bool]](count: size, repeatedValue: [Bool](count: size, repeatedValue: false)) //Graph does not need to have self-connected vertices at init
    }
    
    init(links: [[Bool]]) {
        relations = links
    }
    
    init(connections: [Connection]) {
        let rd = connections.reduce(0) { acc, elem in
            return max(max(elem.0, elem.1), acc)
        }
        let g = Graph(size: rd + 1)
        let newG = g.withConnections(connections)
        relations = newG.result.relations
    }
    
    func withoutConnection(link: Connection) -> ModifiedGraph {
        return modifiedConnection(link, newValue: false)
    }
    
    func withConnection(link: Connection) -> ModifiedGraph {
        return modifiedConnection(link, newValue: true)
    }
    
    func modifiedConnection(link: Connection, newValue: Bool) -> ModifiedGraph {
        if relations[link.a][link.b] == newValue {
            return (self, false)
        }
        
        let mod = relations
            .arrayWithValueAtIndex(link.a, replacedWith: relations[link.a].arrayWithValueAtIndex(link.b, replacedWith: newValue))
            .arrayWithValueAtIndex(link.b, replacedWith: relations[link.b].arrayWithValueAtIndex(link.a, replacedWith: newValue))
        
        
        
        return (Graph(links: mod), true)
    }
    
    func withConnections(links: [Connection]) -> ModifiedGraph {
        return modifiedConnections(links, newValue: true)
    }
    
    func withoutConnections(links: [Connection]) -> ModifiedGraph {
        return modifiedConnections(links, newValue: false)
    }
    
    func modifiedConnections(links: [Connection], newValue: Bool) -> ModifiedGraph {
        var g = self
        var neededAllChanges = true
        
        for link in links {
            let n = g.modifiedConnection(link, newValue: newValue)
            g = n.result
            neededAllChanges = neededAllChanges && n.neededChange
        }
        return (g, neededAllChanges)
    }
    
    func modifiedConnections2(links: [Connection], newValue: Bool) -> ModifiedGraph {
        let initial: ModifiedGraph = (self, true)
        
        let result = links.reduce(initial) { acc, elem in
            let rz = acc.result.modifiedConnection(elem, newValue: newValue)
            return (rz.result, rz.neededChange && acc.neededChange)
        }
        
        return result
    }
    
    func hasImmediateConnection(link: Connection) -> Bool {
        return relations[link.a][link.b]
    }
    
    func immediateVertices(here: Vertex) -> [Vertex] {
        var imm = [Vertex]()
        for vtx in vertices {
            if hasImmediateConnection((a: here, b: vtx)) {
                imm.append(vtx)
            }
        }
        return imm
    }
    
    func immediate2(here: Vertex) -> [Vertex] {
        let vs = relations[here]
        var imm = [Vertex]()
        for (i, v) in enumerate(vs) {
            if i != here && v{
                imm.append(i)
            }
        }
        return imm
    }
    
//    func hasPath(link: Connection) -> Bool {
//        let imms = immediateVertices(link.a)
//        for imm in imms {
//            switch imm {
//            case link.b:
//                return true
//                //            case link.a: //I don't believe this is needed
//                //                break
//            default:
//                let newG = withoutConnection((a: link.a, imm)).result
//                
//                if newG.hasPath((a: imm, link.b)) {
//                    return true
//                }
//            }
//            
//        }
//        return false
//    }
    
    func pointPath(current: Vertex, end: Vertex) -> TraversalResult {
        let imm = current
        
        if imm == end { return (true, [imm, end]) } //We've reached our destination
    
        let pths: TraversalResult = hasPath((a: imm, end))
        
        return (pths.0, pths.1.union(VertexBunch(imm)))
        
    }
    
    func hasPath(link: Connection) -> TraversalResult {
        
        let imms = immediateVertices(link.a)
        
        let immediateConnections: [Connection] = imms.map{ (link.a, $0) }
        
        let newG = self.withoutConnections(immediateConnections).result
        
        let qq = imms.map { newG.pointPath($0, end: link.b) }
        
        let lz = lazy(imms).map { newG.pointPath($0, end: link.b) }
        
        
        let reduced = qq.reduce((false, VertexBunch())) { acc, elem in
            return (elem.0 || acc.0, acc.1.union(elem.1))
        }
        
        return (reduced.0, reduced.1)
        
    }
    
    func hasPath(link: Connection) -> Bool {
        
        let imms = immediateVertices(link.a)
        
        let immediateConnections: [Connection] = imms.map{ (link.a, $0) }
        
        let newG = self.withoutConnections(immediateConnections).result
        
        let lz = lazy(imms).map { newG.pointPath($0, end: link.b) }
        
        for elem in lz {
            if elem.success {
                return true
            }
        }
        
        return false
        
    }
    
    
    func isConnected() -> Bool {
        
        for i in 1 ..< relations.count {
            if !hasPath((0, i)).success {
                return false
            }
        }
        
        return true
    }

}

import QuartzCore

func executionTimeInterval(block: () -> ()) -> CFTimeInterval {
    let start = CACurrentMediaTime()
    block();
    let end = CACurrentMediaTime()
    return end - start
}



let cz: [Connection] = [(0,1),(1,2),(0,4),(3,4),(10, 200)]

//Create more efficient store based off of input array format (i.e. condense 2D bool array into quicker format)
//
////let x = Graph(size: 5).withConnections(cz).result //Cascading results are a good use case for
////let zz = Graph(size: 5).MDFC(cz, newValue: true).result
////x==zz
//
//let x = Graph(connections: cz)
//let qqqq = executionTimeInterval {
//    let h = x.immediate2(10)
//}
//
//let rrrr = executionTimeInterval {
//    x.immediateVertices(10)
//}


//let r = x.isConnected()
//let qwe = x.isConnected2()

//let n = Graph(connections: cz).relations

//let time = executionTimeInterval {
//    x.hasPath((0,3)).reached
//}
//let t2 = executionTimeInterval {
//    x.hasPath((0,3)) as Bool
//}

//Independent function called drawInfo. Pass it a series of points, lines, and labels and it draws it with either default or optional configurations [like format display strings]. Data type will have relative integer coordinates, so that coordinates can be scaled to arbitrary size.
    //Create function for Graph, drawingInfo that creates the data type that drawInfo needs.
    //Data type DrawingPoint with identifier, and xy coordinates. Maybe even do 3D display one day? heheh. 
    //Data type DrawingLine from one DrawingPoint to another. A typealias of a tuple of two, perhaps.



